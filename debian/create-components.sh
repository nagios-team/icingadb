#!/bin/bash

if [ -z "$1" ]; then
    echo "Usage: $0 <ORIGTARGZ>"
    exit 1
fi

UPSTREAM_TARBALL="$(realpath -s "$1")"

if [ ! -e "${UPSTREAM_TARBALL}" ]; then
    echo "Error: Upstream tarball not found"
    exit 1
fi

COMPONENT_NAME="dependencies"
COMPONENT_TARBALL="${UPSTREAM_TARBALL//.orig.tar/.orig-${COMPONENT_NAME}.tar}"

TEMP_DIR="$(mktemp -d)"

GOPATH="${TEMP_DIR}/${COMPONENT_NAME}"
export GOPATH

echo "Unpacking upstream tarball: ${UPSTREAM_TARBALL} into: ${TEMP_DIR}"
tar --strip-components=1 -xaf "${UPSTREAM_TARBALL}" -C "${TEMP_DIR}"

MAIN_DIR="${TEMP_DIR}/cmd/icingadb"

echo "Getting main dependencies into: ${GOPATH}"
cd "${MAIN_DIR}" || exit 1
go get .
cd "${OLDPWD}" || exit 1

MIGRATE_DIR="${TEMP_DIR}/cmd/icingadb-migrate"

echo "Getting migrate dependencies into: ${GOPATH}"
cd "${MIGRATE_DIR}" || exit 1
go get .
cd "${OLDPWD}" || exit 1

#TESTS_DIR="${TEMP_DIR}/tests"
#
#echo "Getting test dependencies into: ${GOPATH}"
#cd "${TESTS_DIR}" || exit 1
#go get -t .
#cd "${OLDPWD}" || exit 1

echo "Fixing permissions for: ${GOPATH}"
chmod -R u+w "${GOPATH}"

echo "Removing unwanted files from: ${GOPATH}"
find "${GOPATH}" -name "*.exe" -print -delete

echo "Creating component tarball: ${COMPONENT_TARBALL}"
cd "${TEMP_DIR}" || exit 1
tar --owner root --group root -caf "${COMPONENT_TARBALL}" "${COMPONENT_NAME}"
cd "${OLDPWD}" || exit 1

echo "Removing temporary directory: ${TEMP_DIR}"
rm -rf "${TEMP_DIR}"
