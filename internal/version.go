package internal

import (
	"github.com/icinga/icinga-go-library/version"
)

// Version contains version and Git commit information.
//
// The placeholders are replaced on `git archive` using the `export-subst` attribute.
var Version = version.Version("1.2.1", "v1.2.1", "1fa236149c11bcc385a466671b0aa03eb82c831c")
